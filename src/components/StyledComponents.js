import styled from "styled-components";

const StyledButton = styled.button`
    background: #7B4CD8;
    color: white;

    font-size: 1em;
    margin: 1em;
    padding: 0.25em 1em;
    border: none;
    border-radius: 3px;
    `
const PrimaryButton = styled.button`
    background: ${props => props.primary ? "palevioletred" : "white"};
    color: ${props => props.primary ? "white" : "palevioletred"};

    font-size: 1em;
    margin: 1em;
    padding: 0.25em 1em;
    border: 2px solid palevioletred;
    border-radius: 3px;
    `
const PropButton = styled.button`
    background: ${props => props.background || "white"};
    color: ${props => props.color || "palevioletred"};

    font-size: 1em;
    margin: 1em;
    padding: 0.25em 1em;
    border: 2px solid palevioletred;
    border-radius: 3px;
    `
export default StyledButton;
export {PropButton, PrimaryButton};