import StyledButton, { PrimaryButton, PropButton } from "./components/StyledComponents";

function App() {
  return (
    <div>
      <StyledButton>Styled Button</StyledButton>
      <br></br>
      <PropButton background="black" color="white">Prop Button 1</PropButton>
      <PropButton background="orange" color="blue">Prop Button 1</PropButton>
      <br></br>
      <PrimaryButton primary>Primary Button</PrimaryButton>
      <PrimaryButton>other button 1</PrimaryButton>
      <PrimaryButton>other button 2</PrimaryButton>
    </div>
  );
}

export default App;
